package com.example.punky_1202162319_si40int_pab_modul1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

   public EditText editAlas;
   public EditText editTinggi;
   public Button buttonCEK;
   public TextView hasil;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editAlas    =   (EditText)findViewById(R.id.alas);
        editTinggi  = (EditText)findViewById(R.id.tinggi);
        buttonCEK   = (Button)findViewById(R.id.buttonCEK);
        hasil       = (TextView)findViewById(R.id.hasil);

        buttonCEK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick (View v){
                hitung(v);
            }
        });
    }

    public void hitung(View view){
        int alas    =   Integer.parseInt(editAlas.getText().toString());
        int tinggi  =   Integer.parseInt(editTinggi.getText().toString());
        int cekHasil    = alas * tinggi;
        hasil.setText("Luasnya adalah" +cekHasil);
    }
}
